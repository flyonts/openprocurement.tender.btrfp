# -*- coding: utf-8 -*-
from logging import getLogger
from openprocurement.api.models import get_now, TZ
from pkg_resources import get_distribution
from openprocurement.api.utils import (
    check_complaint_status,
    remove_draft_bids,
    check_bids,
    context_unpack,
)

PKG = get_distribution(__package__)
LOGGER = getLogger(PKG.project_name)


def check_status(request):
    tender = request.validated['tender']
    now = get_now()
    for complaint in tender.complaints:
        check_complaint_status(request, complaint, now)
    """
    for award in tender.awards:
        for complaint in award.complaints:
            check_complaint_status(request, complaint, now)
    """
    if tender.status == 'active.enquiries' and not tender.tenderPeriod.startDate and tender.enquiryPeriod.endDate.astimezone(TZ) <= now:
        LOGGER.info('Switched tender {} to {}'.format(tender.id, 'active.tendering'),
                    extra=context_unpack(request, {'MESSAGE_ID': 'switched_tender_active.tendering'}))
        tender.status = 'active.tendering'
        return
    elif tender.status == 'active.enquiries' and tender.tenderPeriod.startDate and tender.tenderPeriod.startDate.astimezone(TZ) <= now:
        LOGGER.info('Switched tender {} to {}'.format(tender.id, 'active.tendering'),
                    extra=context_unpack(request, {'MESSAGE_ID': 'switched_tender_active.tendering'}))
        tender.status = 'active.tendering'
        return
    elif tender.status == 'active.tendering' and tender.tenderPeriod.endDate <= now:
        remove_draft_bids(request)
        check_bids(request)
        # belowThresholdRFP after active.tendering goto complete
        numberOfBids = tender.numberOfBids
        if numberOfBids < 2 and tender.auctionPeriod:
            tender.auctionPeriod.startDate = None
        if tender.lots:
            for lot in tender.lots:
                if lot.numberOfBids < 2 and lot.auctionPeriod:
                    setattr(lot.auctionPeriod, 'startDate', None)
                lot.status = 'complete' if lot.numberOfBids else 'unsuccessful'
                numberOfBids += lot.numberOfBids
                LOGGER.info('Switched lot {} of tender {} to {}'.format(lot.id, tender.id, lot.status),
                            extra=context_unpack(request, {'MESSAGE_ID': 'switched_lot_' + lot.status}, {'LOT_ID': lot.id}))
        tender.status = 'complete' if numberOfBids else 'unsuccessful'
        LOGGER.info('Switched tender {} to {}'.format(tender['id'], tender.status),
                    extra=context_unpack(request, {'MESSAGE_ID': 'switched_tender_' + tender.status}))
        return
    elif tender.status in ['active.auction', 'active.qualification', 'active.awarded']:
        # belowThresholdRFP after active.tendering goto complete
        numberOfBids = tender.numberOfBids
        if tender.lots:
            for lot in tender.lots:
                lot.status = 'complete' if lot.numberOfBids else 'unsuccessful'
                numberOfBids += lot.numberOfBids
                LOGGER.info('Switched lot {} of tender {} to {}'.format(lot.id, tender.id, lot.status),
                            extra=context_unpack(request, {'MESSAGE_ID': 'switched_lot_' + lot.status}, {'LOT_ID': lot.id}))
        tender.status = 'complete' if numberOfBids else 'unsuccessful'
        LOGGER.info('Switched tender {} to {}'.format(tender['id'], tender.status),
                    extra=context_unpack(request, {'MESSAGE_ID': 'switched_tender_' + tender.status}))
        return
