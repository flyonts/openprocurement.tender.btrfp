# -*- coding: utf-8 -*-
from openprocurement.api.views.cancellation import TenderCancellationResource
from openprocurement.api.utils import opresource


@opresource(name='Tender RFP Cancellations',
            collection_path='/tenders/{tender_id}/cancellations',
            path='/tenders/{tender_id}/cancellations/{cancellation_id}',
            procurementMethodType='belowThresholdRFP',
            description="Tender cancellations")
class TenderRFPCancellationResource(TenderCancellationResource):
    pass
