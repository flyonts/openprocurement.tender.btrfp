# -*- coding: utf-8 -*-
from openprocurement.api.views.cancellation_document import TenderCancellationDocumentResource
from openprocurement.api.utils import opresource


@opresource(name='Tender RFP Cancellation Documents',
            collection_path='/tenders/{tender_id}/cancellations/{cancellation_id}/documents',
            path='/tenders/{tender_id}/cancellations/{cancellation_id}/documents/{document_id}',
            procurementMethodType='belowThresholdRFP',
            description="Tender cancellation documents")
class TenderRFPCancellationDocumentResource(TenderCancellationDocumentResource):
    pass
