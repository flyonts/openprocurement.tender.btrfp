# -*- coding: utf-8 -*-
from openprocurement.api.views.bid import TenderBidResource
from openprocurement.api.utils import opresource


@opresource(name='Tender RFP Bids',
            collection_path='/tenders/{tender_id}/bids',
            path='/tenders/{tender_id}/bids/{bid_id}',
            procurementMethodType='belowThresholdRFP',
            description="Tender bids")
class TenderRFPBidResource(TenderBidResource):
    pass
