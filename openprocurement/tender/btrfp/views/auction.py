# -*- coding: utf-8 -*-
from openprocurement.api.views.auction import TenderAuctionResource
from openprocurement.api.utils import opresource


@opresource(name='Tender RFP Auction',
            collection_path='/tenders/{tender_id}/auction',
            path='/tenders/{tender_id}/auction/{auction_lot_id}',
            procurementMethodType='belowThresholdRFP',
            description="Tender auction data")
class TenderRFPAuctionResource(TenderAuctionResource):
    pass
