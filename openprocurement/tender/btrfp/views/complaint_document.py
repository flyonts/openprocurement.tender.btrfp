# -*- coding: utf-8 -*-
from openprocurement.api.views.complaint_document import TenderComplaintDocumentResource
from openprocurement.api.utils import opresource


@opresource(name='Tender RFP Complaint Documents',
            collection_path='/tenders/{tender_id}/complaints/{complaint_id}/documents',
            path='/tenders/{tender_id}/complaints/{complaint_id}/documents/{document_id}',
            procurementMethodType='belowThresholdRFP',
            description="Tender complaint documents")
class TenderRFPComplaintDocumentResource(TenderComplaintDocumentResource):
    pass
