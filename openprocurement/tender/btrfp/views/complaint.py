# -*- coding: utf-8 -*-
from openprocurement.api.views.complaint import TenderComplaintResource
from openprocurement.api.utils import opresource


@opresource(name='Tender RFP Complaints',
            collection_path='/tenders/{tender_id}/complaints',
            path='/tenders/{tender_id}/complaints/{complaint_id}',
            procurementMethodType='belowThresholdRFP',
            description="Tender complaints")
class TenderRFPComplaintResource(TenderComplaintResource):
    pass
