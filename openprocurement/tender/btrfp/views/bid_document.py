# -*- coding: utf-8 -*-
from openprocurement.api.views.bid_document import TenderBidDocumentResource
from openprocurement.api.utils import opresource


@opresource(name='Tender RFP Bid Documents',
            collection_path='/tenders/{tender_id}/bids/{bid_id}/documents',
            path='/tenders/{tender_id}/bids/{bid_id}/documents/{document_id}',
            procurementMethodType='belowThresholdRFP',
            description="Tender bidder documents")
class TenderRFPBidDocumentResource(TenderBidDocumentResource):
    pass
