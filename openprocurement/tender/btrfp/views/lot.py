# -*- coding: utf-8 -*-
from openprocurement.api.views.lot import TenderLotResource
from openprocurement.api.utils import opresource


@opresource(name='Tender RFP Lots',
            collection_path='/tenders/{tender_id}/lots',
            path='/tenders/{tender_id}/lots/{lot_id}',
            procurementMethodType='belowThresholdRFP',
            description="Tender lots")
class TenderRFPLotResource(TenderLotResource):
    pass
