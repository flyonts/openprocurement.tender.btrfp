# -*- coding: utf-8 -*-
from openprocurement.api.views.tender import TenderResource
from openprocurement.api.utils import (
    apply_patch,
    context_unpack,
    json_view,
    opresource,
    save_tender,
)
from openprocurement.api.validation import (
    validate_patch_tender_data,
)
from openprocurement.tender.btrfp.utils import (
    check_status,
)


@opresource(name='Tender RFP',
            path='/tenders/{tender_id}',
            procurementMethodType='belowThresholdRFP',
            description="Open Contracting compatible data exchange format. See http://ocds.open-contracting.org/standard/r/master/#tender for more info")
class TenderRFPResource(TenderResource):

    @json_view(content_type="application/json", validators=(validate_patch_tender_data, ), permission='edit_tender')
    def patch(self):
        tender = self.context
        if self.request.authenticated_role != 'Administrator' and tender.status in ['complete', 'unsuccessful', 'cancelled']:
            self.request.errors.add('body', 'data', 'Can\'t update tender in current ({}) status'.format(tender.status))
            self.request.errors.status = 403
            return
        if self.request.authenticated_role == 'chronograph':
            apply_patch(self.request, save=False, src=self.request.validated['tender_src'])
            check_status(self.request)
            save_tender(self.request)
        else:
            apply_patch(self.request, src=self.request.validated['tender_src'])
        self.LOGGER.info('Updated tender {}'.format(tender.id),
                    extra=context_unpack(self.request, {'MESSAGE_ID': 'tender_patch'}))
        return {'data': tender.serialize(tender.status)}
