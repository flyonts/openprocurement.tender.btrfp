# -*- coding: utf-8 -*-
from openprocurement.api.views.tender_document import TenderDocumentResource
from openprocurement.api.utils import opresource


@opresource(name='Tender RFP Documents',
            collection_path='/tenders/{tender_id}/documents',
            path='/tenders/{tender_id}/documents/{document_id}',
            procurementMethodType='belowThresholdRFP',
            description="Tender related binary files (PDFs, etc.)")
class TenderRFPDocumentResource(TenderDocumentResource):
    pass
