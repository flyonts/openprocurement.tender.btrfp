# -*- coding: utf-8 -*-
from openprocurement.api.views.question import TenderQuestionResource
from openprocurement.api.utils import opresource


@opresource(name='Tender RFP Questions',
            collection_path='/tenders/{tender_id}/questions',
            path='/tenders/{tender_id}/questions/{question_id}',
            procurementMethodType='belowThresholdRFP',
            description="Tender questions")
class TenderRFPQuestionResource(TenderQuestionResource):
    pass
