# -*- coding: utf-8 -*-

import unittest

from openprocurement.tender.btrfp.tests import (
    bidder,
    cancellation,
    chronograph,
    complaint,
    document,
    lot,
    tender,
    question,
)


def suite():
    suite = unittest.TestSuite()
    suite.addTest(tender.suite())
    suite.addTest(document.suite())
    suite.addTest(question.suite())
    suite.addTest(complaint.suite())
    suite.addTest(bidder.suite())
    suite.addTest(lot.suite())
    suite.addTest(cancellation.suite())
    suite.addTest(chronograph.suite())
    return suite


if __name__ == '__main__':
    unittest.main(defaultTest='suite')
