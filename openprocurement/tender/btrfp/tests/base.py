# -*- coding: utf-8 -*-
import os
from openprocurement.api.models import SANDBOX_MODE
from openprocurement.api.tests.base import (
    test_tender_data as test_tender_data_api,
    test_features_tender_data as test_features_tender_data_api,
    BaseWebTest as BaseWebTestApi,
    BaseTenderWebTest as BaseTenderWebTestApi,
)


test_tender_data = test_tender_btrfp_data = test_tender_data_api.copy()
test_tender_data['procurementMethodType'] = "belowThresholdRFP"
test_features_tender_data = test_features_tender_data_api.copy()
test_features_tender_data['procurementMethodType'] = "belowThresholdRFP"


if SANDBOX_MODE:
    test_tender_data['procurementMethodDetails'] = 'quick, accelerator=1440'


class BaseWebTest(BaseWebTestApi):
    relative_to = os.path.dirname(__file__)


class BaseTenderWebTest(BaseTenderWebTestApi):
    initial_data = test_tender_data
    relative_to = os.path.dirname(__file__)

    def create_tender(self):
        super(BaseTenderWebTest, self).create_tender()
        response = self.app.get('/tenders/{}'.format(self.tender_id))
        self.assertEqual(response.status, '200 OK')
        self.assertEqual(response.content_type, 'application/json')
        tender = response.json['data']
        self.assertEqual(tender['procurementMethodType'], 'belowThresholdRFP')
